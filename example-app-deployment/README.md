# How to set up authentication for a new app

## Preparation

1. Set upp and deploy your app. Expose the app as a
   service, *not* as a route.
1. Verify you can access your app
   `kubectl port-forward -n MY_NAMESPACE svc/MY_SERVICE`
1. Decide domainname (myapp.jobtechdev.se), and
   [register in DNS](https://gitlab.com/arbetsformedlingen/devops/crossplane/crossplane-infra#create-a-dns-record). Document assumes it is a subdomain of jobtechdev.se.
1. Create client secret, save for later. The secret
   *MUST* be different for each environment.
1. Create a cookie secret with length 16 or 32 characters.
   It *MUST NOT* be the same as client secret and *MUST* be
   different for each environment. Save for later.

## Register your app in OpenID Connect providern

In the secret dex-secret(namespace dex-test/dex-prod) add an environment variable DEX_APPNAME_SECRET with your client secret above.

Check out the dex-infra repository and edit the kustomize/base/config.yaml. Add a section under `staticClients`:

```yaml
- id: appname
  redirectURIs:
  - 'https://appname.jobtechdev.se/oauth2/callback'
  name: 'appname'
  secret: {{ .Env.DEX_APPNAME_SECRET }}
```

`id` shall be your apps name without spaces. In the `redirectUTIs`
set the domain name for your app. `name` shall be your apps name
easy to read for normal user. Finally secret shall reference to
the client secret environment variable created above.

Create a merge request of your change and ask anyone in Calamari
to review.

Once the changes are deployed continue on the next section.

## Configure proxy for your app

The proxy is preferably running in the same namespace of your app,
this document assumes it.

Create a secret in the namespace named oauth2-proxy. it shall set
two environment variables:

* `OAUTH2_PROXY_CLIENT_SECRET` with the client secret from the
  preparations.
* `OAUTH2_PROXY_COOKIE_SECRET` with the cookie secret from the
  preparations.

Copy the files `oauth2-proxy.cfg`, `proxy-deployment.yaml`, and
`proxy-route.yaml` to the infra repository of your app.

In the `proxy-route.yaml`, change the following field in the Route:

* `metadata.spec.host` set your dns name.

For the DnsRecord:

* `metadata.name` to a name matching your DNS name.
* `spec.parameters.name` to match the first part of your DNS name.

In the configuration file oauth2-proxy.cfg change the following
variables:

* `redirect_url` - set the domain name to your domain name.
* `upstreams` - change the hostname to the name of your service.
* `client_id` - to the same id as you tou sat for the app in
  the proxy.

Add the files yaml to `kustomization.yaml` under the `resources` section. Do also add a config-map generator for the config file:

```yaml
configMapGenerator:
- name: oauth2-proxy-config-file
  files:
  - oauth2-proxy.cfg
```

Apply config and verify your configuration.
