# Setup ArgoCD to pull Dex IDP to cluster.

You not always want or can give the build environment access to
clusters where products shall be deployed. In those cases 
ArgoCD can help. RedHat OpenShift offers ArgoCD via the GitOps operator.

## Setup

If Openshift Gitops operator is not installed on the cluster, install it. 
This only needs to do once for each cluster and it handle all projects
using ArgoCD to keep projects up to date.

Create a Project for your product. In this case `dex-prod`.

To your namespace/project apply the `targocd-deployer-access-to-project.yaml`.
It gives ArgoCD/GitOps operator access to deploy to the namespace.

Next, apply the `prod.yaml`. It applies in to the ArgoCD/GitOps operators 
namespace and tell ArgoCD to monitor the dex-infra repository and
deploy changes to the cluster. 

