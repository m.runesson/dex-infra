# Interaction

## Login flow

```mermaid
sequenceDiagram
Client->>Proxy: Get MyService
Proxy-->>Client: 302 Redirect to Dex
Client->>Dex: Get Dex (clientId & Scope)
Dex-->>Client: Login page
Client->>Dex: Filled in login form
Dex->>Ldap: Authenticate
Ldap-->>Dex: OK
Dex-->>Client: Approval page
Client->>Dex: Approve click
Dex-->>Client: 302 Redirect with auth code
Client->>Proxy: Get redirect Url with auth code
Proxy->>Dex: Validate Auth code
Dex-->>Proxy: OK
Proxy-->>Client: Redirect to org page (JWT)
Client->>Proxy: Get MyService (inc auth header)
Proxy->>MyService: Get MyService (inc auth header)
MyService-->>Proxy: First page
Proxy-->>Client: First page
```
